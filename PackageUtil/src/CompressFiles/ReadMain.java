package CompressFiles;

import Exceptions.FileException;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by guoweiying on 16/7/15.
 */
public class ReadMain {
    public static void main(String[] args) throws FileException{
        File rootFile = new File(args[0]);
        //File rootFile = new File("/Users/guoweiying/Documents/HJD666666666");
        List<File> fileList;

        System.out.println(System.currentTimeMillis());

        try {
            WriteFile.CreateFile(args[1]);
            if (rootFile.exists()) {
                ReadUtil readUtil = new ReadUtil(rootFile);
                fileList = readUtil.CountFile();
                //for (File file: fileList) {
                //    System.out.println(file.getAbsolutePath() + " " + file.length());
                //}
                //
                //System.out.println(fileList.size());
            }
            else throw new FileException("文件目录不存在！");
            WriteFile writeFile = new WriteFile(fileList, rootFile.getCanonicalPath());
            writeFile.WriteIntoCom();
        } catch (FileException e) {
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(System.currentTimeMillis());
    }
}
