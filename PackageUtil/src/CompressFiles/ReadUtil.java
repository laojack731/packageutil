package CompressFiles;

import java.io.File;
import java.util.*;

/**
 * Created by guoweiying on 16/7/18.
 */

public class ReadUtil {
    private Map<String, String> fileMap;
    private List<File> dirList;
    private List<File> fileList;

    public ReadUtil(File rootFile) {
        fileList = new ArrayList<File>();
        dirList = new ArrayList<File>();
        dirList.add(rootFile);
    }

    public List<File> CountFile() {
        for (int i = 0; i < dirList.size(); ++i){
            File file = dirList.get(i);
            File[] subFiles = file.listFiles();
            for (File subFile : subFiles) {
                if (subFile.isDirectory()) {
                    dirList.add(subFile);
                } else {
                    fileList.add(subFile);
                }
            }
        }
        Collections.sort(fileList, new StringCom());
        return fileList;
    }

    static class StringCom implements Comparator {
        public int compare(Object object1, Object object2) {
            File f1 = (File) object1;
            File f2 = (File) object2;

            String fpath1 = f1.getAbsolutePath();
            String fpath2 = f2.getAbsolutePath();
            return fpath1.compareTo(fpath2);
        }
    }
}
