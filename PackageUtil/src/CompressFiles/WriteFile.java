package CompressFiles;

import Exceptions.FileException;

import java.io.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by guoweiying on 16/7/18.
 */
public class WriteFile {
    private final static String crlf = System.getProperty("line.separator");
    private static File comFile;
    private static RandomAccessFile rf_comFile;
    private String rootFilePath;
    private List<File> fileList;
    private Map<String, Long> fileMap;

    public WriteFile(List<File> fileList, String rootFilePath) {
        this.fileList = fileList;
        this.rootFilePath = rootFilePath;
        fileMap = new HashMap<String, Long>();
    }

    public static void CreateFile(String fileName) throws FileException{
        //System.out.println(System.getProperty("user.dir") + "/Compress.dat");
        comFile = new File(System.getProperty("user.dir") + "/data/" + fileName);
        if (!comFile.exists()) {
            try {
                comFile.createNewFile();
            } catch (IOException e) {
                System.out.println("文件创建失败");
            }
            try {
                rf_comFile = new RandomAccessFile(comFile, "rw");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        else {
            throw new FileException("资源包已存在");
        }
    }

    public File WriteIntoCom() {
        Long offset = new Long(8);
        try {
            rf_comFile.seek(offset);
            for (File file: fileList) {
                String filePath = file.getCanonicalPath();
                filePath = filePath.substring(rootFilePath.length());
                fileMap.put(filePath, offset);

                rf_comFile.writeInt(filePath.length());
                rf_comFile.writeBytes(filePath);
                rf_comFile.writeLong(file.length());

                System.out.println(filePath + " " + file.length());

                offset = WriteFile(file);
            }
            WriteMap();
            rf_comFile.seek(0);
            rf_comFile.writeLong(offset);
            //System.out.println(offset);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return comFile;
    }

    public Long WriteFile(File file) throws IOException {
        BufferedInputStream outbs = new BufferedInputStream(new FileInputStream(file));
        byte[] buf = new byte[1024];
        int len;
        while ((len = outbs.read(buf)) > 0) {
            rf_comFile.write(buf, 0, len);
        }
        if (outbs != null) outbs.close();
        return rf_comFile.getFilePointer();
    }

    public void WriteMap() throws IOException {
        rf_comFile.writeInt(fileMap.size());
        for (Map.Entry<String, Long> entry: fileMap.entrySet()) {
            String filePath = entry.getKey();
            //System.out.println(filePath.length() + " " + filePath + " " + entry.getValue());
            rf_comFile.writeInt(filePath.length());
            rf_comFile.write(filePath.getBytes());
            rf_comFile.writeLong(entry.getValue());
        }
    }
}
